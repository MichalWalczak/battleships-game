#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include<winsock2.h>   
#include<windows.h> 

#include "ships.h"
#define NO_OF_PARAMETERS 10
#define LISTENIG_PORT 8888 
#define SENDINIG_PORT 1030

//192.168.0.101

extern char player_brd[B_SIZE][B_SIZE], enemy_brd[B_SIZE][B_SIZE], enemy_dup_brd[B_SIZE][B_SIZE];

void quick_ships_entering() //testing
{
	int i=0, j=0;
	initialize_board(player_brd, sizeof(player_brd));
	for(j=2;j<7;j++)
	{
		player_brd[1][j]= (char)SHIP;
	}
	for(j=2;j<6;j++)
	{
		player_brd[3][j]= (char)SHIP;
	}
	for(j=2;j<5;j++)
	{
		player_brd[5][j]= (char)SHIP;
	}
	for(j=2;j<5;j++)
	{
		player_brd[7][j]= (char)SHIP;
	}
	for(j=2;j<4;j++)
	{
		player_brd[9][j]= (char)SHIP;
	}
}

int main(int argc, char *argv[]) 
{
	//void recive_data_from_enemy(SOCKET, int*, char*, int, int);
	
	char *buffer;
	char *moves_buffer;
	char char_to_int;
	char ac[80];
	int i=0, j=0, z=0, read_success=0, menu_option=0, ships_deployed=0, enemy_board_rdy=0, game_over=0, enemy_or_player_move=0;
	int enemy_address_struct_size=0, recv_data_lenth=0, known_enemy=0, connection=0, ships_sended=0, quick_fill=0, x_pin_point=0, y_pin_point=0; 
	struct hostent *phe;
	struct in_addr addr;
	struct sockaddr_in player_addr; 
	struct sockaddr_in enemy_addr;   // addres structure for comunication with enemy
	
	SOCKET s, *enemy_socket;    // player sending socket s & enemy sending socket enemy_socket
    HANDLE handle_wait_for_response_thread=NULL; // new thread to wait for enemy moves
	enemy_socket=(SOCKET *)malloc(sizeof(SOCKET)); 
	enemy_address_struct_size=sizeof(enemy_addr);
	buffer = (char *)malloc(BUFFER_SIZE*sizeof(char));
	moves_buffer = (char *)malloc(BUFFER_SIZE*sizeof(int));
	WSADATA wsa;
	
	if (WSAStartup(MAKEWORD(2,2),&wsa) != 0)
       printf("Initialise failed errno %d",WSAGetLastError());
	else 
       printf("Initialised.");
    gethostname(ac, sizeof(ac));
    phe = gethostbyname(ac);
    if (phe == 0) 
    printf("Error get");
	else 
	{
	    for (i = 0; phe->h_addr_list[i] != 0; ++i) 
		{
	        memcpy(&addr, phe->h_addr_list[i], sizeof(struct in_addr));
	        printf("Address %d : %s ",i, inet_ntoa(addr) );
	    }
	}
    
    
    if((s = socket(AF_INET , SOCK_DGRAM , 0 )) == INVALID_SOCKET)
      printf("Could not create socket : %d" , WSAGetLastError());
 	printf("Socket created.\n");
 	
	//Prepare the sockaddr_in structure for player
    player_addr.sin_family = AF_INET; // IP_v4
    player_addr.sin_addr.s_addr = inet_addr(inet_ntoa(addr)); // 
    player_addr.sin_port = htons( LISTENIG_PORT );
    
    //Prepare the sockaddr_in structure for the enemy
    enemy_addr.sin_family = AF_INET; // IP_v4
    enemy_addr.sin_addr.s_addr =  INADDR_ANY;; 
    enemy_addr.sin_port = htons( LISTENIG_PORT );
	
    if( bind(s ,(struct sockaddr *)&player_addr , sizeof(player_addr)) == SOCKET_ERROR)
    {
        printf("Bind failed with error code : %d" , WSAGetLastError());
        //exit(EXIT_FAILURE);
    }
    puts("Bind done");
	
	initialize_board(enemy_dup_brd, sizeof(enemy_dup_brd));
    initialize_board(player_brd, sizeof(player_brd));
    initialize_board(enemy_brd, sizeof(enemy_brd));

	do
	{
		printf("\n\n\t\tMAIN MENU\n\n");	
		printf("\t1 - enter_ships\n");
		printf("\t2 - send board & rec enemy brd\n");
		printf("\t3 - begin_game\n");
		printf("\t4 - save_board_to_file \n");
		printf("\t5 - read_board_from_file \n");
		printf("\t6 - end program\n\n\t");
		read_success=scanf("%d",&menu_option);
		fflush(stdin);
		if(read_success!=0) 
		{
			switch (menu_option)
			{
				case 1:
				{	
					printf("\n quick fill?\n1 - YES\n2 - NO");
					scanf("%d",&quick_fill);
					fflush(stdin);
					if (quick_fill==2)
					{
						ships_deployed = enter_ships(buffer, player_brd);
					}
					else if(quick_fill==1)  //testing
					{
						quick_ships_entering();//testing
						ships_deployed=1;
					}
				}
				break;
				case 2:
				{
					connection=0; 
					known_enemy=0;
					ships_sended=0;
					enemy_board_rdy=0;
					do
					{
						if(connection==0)
						{
							printf("Do you know your enemy? And want to send him your board\n 1 - YES \n 2 - NO, I want to wait for enemy board\n 3 - NO, I Want to play with AI \n");
							scanf("%d",&known_enemy);
						}
						if (known_enemy==1)
						{
							if(connection==0)
							{
								fflush(stdin);
								printf("enter enemy ip adres in form 192.168.0.109 \n");
								scanf("%s",&ac);
								enemy_addr.sin_addr.s_addr = inet_addr(ac);
							}
							memset(buffer,'\0',BUFFER_SIZE);
							z=0;
							for(i=1;i<B_SIZE;i++)
							{
								for(j=1;j<B_SIZE;j++)
								{
									buffer[z] = player_brd[i][j];
									z++;
								}
							}
					        if (sendto(s, buffer, sizeof(player_brd), 0, (struct sockaddr*) &enemy_addr, enemy_address_struct_size) == SOCKET_ERROR)
					        {
						            printf("sendto() failed with error code : %d\n" , WSAGetLastError());
					        }
					        else
					        {
					        	ships_sended=1;
					        	known_enemy=2;
					        	connection=1;
								enemy_or_player_move=1;
					       	}
						}
						else if(known_enemy==2)
						{
							printf("Waiting for data\n");
							do
							{
								if ((recv_data_lenth = recvfrom(s, buffer, sizeof(enemy_brd), 0, (struct sockaddr *) &enemy_addr, &enemy_address_struct_size)) == SOCKET_ERROR)
		    					{
		        					printf("recvfrom() failed with error code : %d\n" , WSAGetLastError());
		    					}
		    					else
								{
									printf("Received packet from %s:%d\n", inet_ntoa(enemy_addr.sin_addr), ntohs(enemy_addr.sin_port));
		    						z=0;
									for(i=1;i<B_SIZE;i++)
									{
										for(j=1;j<B_SIZE;j++)
										{
											enemy_brd[i][j]=buffer[z];
											z++;
										}
									}
		    						enemy_board_rdy=1;
									connection=1;
									known_enemy=1;
									enemy_or_player_move=1;
								}
							}while(recv_data_lenth==0);	
						}
						else if(known_enemy==3)
						{
							memcpy(enemy_brd, player_brd, sizeof(player_brd)); //testing
							enemy_board_rdy=1, ships_sended=1; enemy_or_player_move=1;
						}
					}while((enemy_board_rdy==0) || (ships_sended==0));
					
				}	
				break;					
				case 3:
				{
					fflush(stdin);
					if (ships_deployed==1)
					{
						if(enemy_board_rdy==1)
						do
						{		
							display_board(player_brd);
							if(enemy_or_player_move==1)
							{
								game_over=player_turn(&buffer, &x_pin_point, &y_pin_point);	 // F13
								if(known_enemy!=3)
								{
   									memset(moves_buffer,'\0',BUFFER_SIZE);
 									moves_buffer[0] =(char)( x_pin_point +'0'); // change int to char in order to send data '0' is 48 in ascii 
									moves_buffer[1] =(char)( y_pin_point +'0');	// basicly it means pe 7 + 48 -> send ascii char 55

									if (sendto(s, moves_buffer, 2*sizeof(char), 0, (struct sockaddr*) &enemy_addr, enemy_address_struct_size) == SOCKET_ERROR)
							        {
								            printf("sendto() failed with error code : %d\n" , WSAGetLastError());
							        }
						        
							        fflush(stdin);
								}
								enemy_or_player_move=0;
							}
							else if	(enemy_or_player_move==0)
							{
								
								if(known_enemy!=3)
								{
									memset(moves_buffer,'\0',BUFFER_SIZE);
									do
									{
										if ((recv_data_lenth = recvfrom(s, moves_buffer, 2*sizeof(char), 0, (struct sockaddr *) &enemy_addr, &enemy_address_struct_size)) == SOCKET_ERROR)
				    					{
				        					printf("recvfrom() failed with error code : %d\n" , WSAGetLastError());
				    					}
				    					else
				    					enemy_or_player_move=1;
				    					
											x_pin_point=moves_buffer[0]-'0'; // decode in order to use data, sended ascii char pe 55 - 48 -> use number 7
											y_pin_point=moves_buffer[1]-'0';	
	
									}while(enemy_or_player_move==0);
									
								}
								printf("\n sending enemy codrs %d, %d", x_pin_point, y_pin_point);
								enemy_or_player_move=1;
								game_over=enemy_turn(&buffer,  &x_pin_point, &y_pin_point, known_enemy);
							}
								
						}while(game_over==0);
						else
						printf("\n Enemy board is not ready");
					}
					else 
					printf("\n Your ships are not deployed");
					
					if(game_over==1)
					printf("\n uuuuu congratz");
					else if(game_over==2)
					printf("\n buuuuu u suck");
				}
				break;
				case 4:
				{
					printf("\t4 - save_equation_to_file\n\t");
				}
				break;
				case 5:
				{
					printf("\t5 - read_equation_from_file\n\t");
					if (read_boards_from_file(player_brd))
					ships_deployed=1;	
				}
				break;					
				case 6:
				{
					printf("\t6 - the end\n");
					break;
				}
				default:
				{
					printf("\t invalid entry\n");
					fflush(stdin);
				}
				break;	 
			}
		}
		else
		{
			printf("\t invalid entry\n");
			fflush(stdin);
		}
	}while(menu_option!=5);	


		
	closesocket(s);
	WSACleanup();
	
	free(enemy_socket);
	free(moves_buffer);
	free(buffer);
	system("pause");	
	return 0;
}




