#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "ships.h"

int get_pin_point_cords(int, char []);   /* returns row or col number; arg1 = 0 for col arg1=1 for rows*/
void user_entery(char *, int *, int *);   /* allow user to enter coodrdinates and validates them  */
void place_a_dot(int, int, int, char [B_SIZE][B_SIZE]); /* function leaves a mark on the board 	*/	
void place_a_fake_dot(int, char [B_SIZE][B_SIZE], int, int); /* leaves a mark on the duplicated board */
int get_ships_count(int, char**); // returns no of part and type of ship being deployed 
int place_validation(char [B_SIZE][B_SIZE], int, int, int); // returns 1 if place is valid 0 if it's not
void ava_to_block(char [B_SIZE][B_SIZE], char [B_SIZE][B_SIZE]); //after deploing full unit changes board
int check_hit_count(); // checks the number of hits returns 1 if no. of parts = no. of hits, 0 otherwise 

char player_brd[B_SIZE][B_SIZE], enemy_brd[B_SIZE][B_SIZE], enemy_dup_brd[B_SIZE][B_SIZE];
char col_symbol[B_SIZE] = {'x', '0', '1', '2', '3', '4', '5', '6', '7', '8', '9'};
char row_symbol[B_SIZE] = {'x', 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J'};


// F1 ====================================================+====================================================	
int get_pin_point_cords(int x_or_y, char pin_point[2]) //arg1=0 -> col arg1=1 -> rows
{

	int i=0, j=0;
	if(x_or_y == 0)
	{
		pin_point[x_or_y]=toupper(pin_point[x_or_y]);
		for(i=0;i<B_SIZE;i++)
		{
			if(pin_point[x_or_y]==col_symbol[i])
			return i;
		}
		return 0; //invalid entry
	}
	else if(x_or_y == 1)
	{
		pin_point[x_or_y]=toupper(pin_point[x_or_y]);
		for(j=0;j<B_SIZE;j++)
		{
			if(pin_point[x_or_y]==row_symbol[j])
			return j;
		}
		return 0; //invalid entry
	}
}
// F2 ====================================================+================================================
void user_entry(char *buffer, int *y_pin_point, int *x_pin_point)
{
	short int read_success=0, i=0;
	char pin_point[2];
	printf("\nEnter coordinates %s",buffer);
	do
	{
		printf("\nEnter col: ");
		if(scanf("%1c",&pin_point[0])==1)
		{
			if((*x_pin_point=get_pin_point_cords(0, pin_point))==0)
			printf("\nNo such column");
			else
			read_success=1;
		}
		else
		printf("\nInvalid entry");
		fflush(stdin);						
	}while(read_success==0)	;
	read_success=0;
	do
	{
		printf("\nEnter row: ");
		if(scanf("%1c",&pin_point[1])==1)
		{
			if((*y_pin_point=get_pin_point_cords(1, pin_point))==0)
			printf("\nNo such row");
			else
			read_success=1;
		}
		else
		printf("\nInvalid entry");
		fflush(stdin);						
	}while(read_success==0)	;
}
// F3 ====================================================+================================================
void initialize_board(char player_brd[B_SIZE][B_SIZE], int size)
{
	int i=0,j=0;
	memset(player_brd,'.',size);
	for(j=0;j<B_SIZE;j++)
	{
		player_brd[0][j]=col_symbol[j];
	}
	for(i=0;i<B_SIZE;i++)
	{	
		player_brd[i][0]=row_symbol[i];
	}	
}
// F4 ====================================================+=================================================
void display_board(char player_brd[B_SIZE][B_SIZE])			/* displays board in fashionable manner	*/
{
	int i=0, j=0;
	system("cls");
	
	for(i=0;i<B_SIZE;i++)
	{
		printf("\n");
		for(j=0;j<B_SIZE;j++)
		{
			printf("%c ",player_brd[i][j]);
		}
		printf("\t\t");
		for(j=0;j<B_SIZE;j++)
		{
			printf("%c ",enemy_dup_brd[i][j]);
		}
		
	}
	printf("\n");
}
// F5 ====================================================+=================================================
int enter_ships(char *buffer, char player_brd1[B_SIZE][B_SIZE])	/*ships are	1x5, 1x4, 2x3, 1x2		*/
{
	int num_ships=0, x_pin_point=0, y_pin_point=0, num_of_units=0;
	do
	{
		num_of_units = get_ships_count(num_ships, &buffer);
		if((num_ships>0) && (num_of_units==1))
		{
			ava_to_block(player_brd, enemy_dup_brd);
		}		
		display_board(enemy_dup_brd);
		
		printf("\nDeploing ship part no = %d", num_of_units);
	
		user_entry(buffer, &x_pin_point, &y_pin_point);
		
		if (place_validation(enemy_dup_brd,x_pin_point,y_pin_point, num_of_units)!=0)
		{
			place_a_dot(SHIP, x_pin_point, y_pin_point, player_brd);
			place_a_fake_dot(SHIP,enemy_dup_brd, x_pin_point, y_pin_point);
			num_ships++;
		}
		else
		printf("\ncan't place there");
	}while(num_ships<NUMBER_OF_SHIP_PARTS);
	ava_to_block(player_brd, enemy_dup_brd);
	initialize_board(enemy_dup_brd, sizeof(enemy_dup_brd));
	return 1;
}
// F6 ====================================================+===============================================
void ava_to_block(char player_brd[B_SIZE][B_SIZE], char board_dup[B_SIZE][B_SIZE])
{
	int i=0, j=0;
	for(i=0;i<B_SIZE;i++)
	{
		for(j=0;j<B_SIZE;j++)
		{
			if(board_dup[i][j]==(char)AVA)
			board_dup[i][j]=(char)BLOCK;
			if(board_dup[i][j]==(char)DOT) // if place is a '.' check bezel spots
			{
				if(board_dup[i+1][j+1]==(char)SHIP) //place blok 'X' in right upper corner near the ship
				board_dup[i][j]=(char)BLOCK;
				if(board_dup[i-1][j+1]==(char)SHIP) //place blok 'X' in right lower corner near the ship
				board_dup[i][j]=(char)BLOCK;
				if(board_dup[i+1][j-1]==(char)SHIP) //place blok 'X' in left upper corner near the ship
				board_dup[i][j]=(char)BLOCK;
				if(board_dup[i-1][j-1]==(char)SHIP) //place blok 'X' in left lower corner near the ship
				board_dup[i][j]=(char)BLOCK;
			}
		}
	}
}
// F7 ====================================================+===============================================
int place_validation(char brd[B_SIZE][B_SIZE], int x_pp, int y_pp, int control_no)
{
	if(control_no==1) // first 5 control numbers reserved for ship deployment F5 
	{
		printf("control no %d", control_no);
		if((brd[x_pp][y_pp] == (char)DOT) ) 
		return 1;
		else return 0;
	}
	else if((control_no>1) && (control_no<6))
	{
		printf("control no %d", control_no);
		if((brd[x_pp][y_pp] == (char)AVA) )
		{	return 1;	printf("brd[x_pp][y_pp] %c", brd[x_pp][y_pp]);	}
		else return 0;
	}
	else if(control_no == 6) //control number 6 reserved for player turn sooting phase F13
	{	
		if(enemy_dup_brd[x_pp][y_pp]==(char)HIT)
		return 0;
		else if(enemy_dup_brd[x_pp][y_pp]==(char)MISS)
		return 0;
		else if((brd[x_pp][y_pp] == (char)SHIP))
		return 1;	
		else if((brd[x_pp][y_pp] == (char)DOT))
		return 2;	
		else return 0;	
	}
	else if(control_no == 7) //control number 7 reserved for enemy turn sooting phase F14
	{
		if(brd[x_pp][y_pp]==(char)HIT)
		return 0;
		else if(brd[x_pp][y_pp]==(char)MISS)
		return 0;
		else if((brd[x_pp][y_pp] == (char)SHIP))
		return 1;	
		else if((brd[x_pp][y_pp] == (char)DOT))
		return 2;	
		else return 0;
	}
	else
	return 0;	
}
// F8 ====================================================+==================================================
int get_ships_count(int num_ships, char **buffer) // returns no of part and type of ship being deployed 
{
	int part_number=0;
	
		if((num_ships>=0) && (num_ships < AIR))
		{
			part_number = (num_ships)+1; //ship parts count starts from 1
			*buffer="to deploy Aircraft Carrier (5 parts)"; 
		}
		if( (num_ships>=AIR) && (num_ships< (AIR+BAT) ) )
		{
			part_number=(num_ships-AIR)+1; //count starts from 1 but - size of Aircraft
			*buffer="to deploy Battleship (4 parts)"; 
		}		
		if( (num_ships>=AIR+BAT) && (num_ships< AIR+BAT+SUB))
		{
			part_number=(num_ships-AIR-BAT)+1; // - size of Aircraft & Battleship
			*buffer="to deploy Submarine (3 parts)"; 
		}
		if( (num_ships>=AIR+BAT+SUB) && (num_ships< (AIR+BAT+SUB+CRU) ) )
		{
			part_number=(num_ships-AIR-BAT-SUB)+1;//- rest of ships
			*buffer="to deploy Cruiser (3 parts)"; 
		}
		if( (num_ships>=AIR+BAT+SUB+CRU) && (num_ships<NUMBER_OF_SHIP_PARTS))
		{
			part_number=(num_ships-AIR-BAT-SUB-CRU)+1; //- rest of ships
			*buffer="to deploy Destroyer (2 parts)";
		}
	return part_number;
}
// F9 ======/*   arg1-> 0-ship, 1-hit, 2-block'X', 3-miss, 4-avaliable spot, 5-'.'	*/=====================
void place_a_fake_dot(int dot_type, char brd[B_SIZE][B_SIZE],  int x_pin_point, int y_pin_point)	
{
	fflush(stdin);
	brd[x_pin_point][y_pin_point]=(char)dot_type;

	//marks places available for ship deployment
	if((x_pin_point>1) && ((brd[x_pin_point-1][y_pin_point])==(char)DOT)) 
	brd[x_pin_point-1][y_pin_point]=(char)AVA;
	if((x_pin_point<11)&& ((brd[x_pin_point+1][y_pin_point])==(char)DOT))
	brd[x_pin_point+1][y_pin_point]=(char)AVA;
	if((y_pin_point>1) && ((brd[x_pin_point][y_pin_point-1])==(char)DOT))
	brd[x_pin_point][y_pin_point-1]=(char)AVA;
	if((y_pin_point<11)&& ((brd[x_pin_point][y_pin_point+1])==(char)DOT))
	brd[x_pin_point][y_pin_point+1]=(char)AVA;
}
// F10 ==========/*   arg1-> 0-ship, 1-hit, 2-block'X', 3-miss, 4-avaliable spot, 5-'.'	*/=====================
void place_a_dot(int dot_type, int x_pin_point, int y_pin_point, char brd[B_SIZE][B_SIZE])	
{
	fflush(stdin);
	brd[x_pin_point][y_pin_point]=(char)(dot_type);
}
// F11 ===================================================+====================================================
int save_boards_to_file(char player_brd[B_SIZE][B_SIZE])		/* allow user to save board to file		*/
{
}
// F12 ===================================================+====================================================
int read_boards_from_file(char player_brd[B_SIZE][B_SIZE])		/* allow user to read board from file	*/
{
}
// F13====================================================+====================================================
int player_turn(char **buffer, int *x_pin_point, int *y_pin_point)
{
	int x_pp=0, y_pp=0, hit_or_miss=0, game_over=0;
	*buffer="to shoot";
	//display_board(player_brd);
	do
	{
		user_entry(*buffer, &x_pp, &y_pp);
		hit_or_miss=place_validation(enemy_brd, x_pp, y_pp, 6);//0-invalid, 1-hit, 2-miss; 6 is control no. for F13 
	
	}while(hit_or_miss==0);

	if(hit_or_miss==1)
	{
		place_a_dot(HIT, x_pp, y_pp, enemy_dup_brd);
		game_over=check_hit_count();
	}
	else if(hit_or_miss==2)
	{
		place_a_dot(MISS, x_pp, y_pp, enemy_dup_brd);
	}
	*x_pin_point = x_pp; // pewnie mozna lepej
	*y_pin_point = y_pp;

	return game_over;
}
// F14====================================================+====================================================
int enemy_turn(char **buffer, int *x_pin_point, int *y_pin_point, int op_type)
{
	int control_no=0, game_over=0, hit_or_miss=0;

	if(op_type==3)
	{
		do
		{
			*x_pin_point=(rand()%10)+1; *y_pin_point=(rand()%10)+1;
			// 7 is the control no. for enemy turn shooting phase
			hit_or_miss=place_validation(player_brd, *x_pin_point, *y_pin_point, 7); 
		}while(hit_or_miss==0);
	}
	else 
	{
		hit_or_miss=place_validation(player_brd, *x_pin_point, *y_pin_point, 7);
	}
	
	if(hit_or_miss==1)
	{
		place_a_dot(HIT, *x_pin_point,  *y_pin_point, player_brd);
		game_over=check_hit_count();
	}
	else if(hit_or_miss==2)
	{
		place_a_dot(MISS, *x_pin_point,  *y_pin_point, player_brd);
	}
	return game_over;
}	
// F15====================================================+====================================================
int check_hit_count()
{
	int i=0, j=0, hits_counter=0, enemy_hits_counter=0;
	for(i=0;i<B_SIZE;i++)
	{
		for(j=0;j<B_SIZE;j++)
		{
			if(enemy_dup_brd[i][j]==(char)HIT)
			hits_counter++;
			if(player_brd[i][j]==(char)HIT)
			enemy_hits_counter++;
		}
	}
	if(hits_counter>=NUMBER_OF_SHIP_PARTS)
		return 1;
	else if(enemy_hits_counter>=NUMBER_OF_SHIP_PARTS)
		return 2;
	else
		return 0;
}
// F16====================================================+====================================================



