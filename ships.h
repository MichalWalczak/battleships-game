/* This is what i have to say about my project:

"Lorem ipsum dolor sit amet, consectetur adipiscing elit,
sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. 
Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris
nisi ut aliquip ex ea commodo consequat."

*/

/*=================================================================+=============================
 						includes & defines
==================================================================+=============================*/

#include <stdio.h>
#include <stdlib.h>

#define B_SIZE 11 // 10x10 board with aditional space to mark col & rows 
#define BUFFER_SIZE 512 // size of buffer used to show various msgs
#define NUMBER_OF_SHIP_PARTS 17 // number that represent all avaliable ship parts 
#define SHIP 219 // number of char in ASCII that will represent ship 
#define BLOCK 88 // number of char in ASCII that will represent place u can't place ship
#define HIT 1 // number of char in ASCII that will represent ship being hit
#define MISS 158 // number of char in ASCII that will represent missed shot
#define AVA 177 // number of char in ASCII that will represent available spot to place a ship
#define DOT 46 // number of char in ASCII that will represent all free spots
#define AIR 5//SIZE_OF_AIRCRAFT_CARRIER
#define BAT 4//SIZE_OF_BATTLESHIP
#define SUB 3//SIZE_OF_SUBMARINE
#define CRU 3//SIZE_OF_CRUISER
#define DES 2//SIZE_OF_DESTROYER


/*===============================================================+===============================
						methodes
================================================================+===============================*/

int enter_ships(char *, char [B_SIZE][B_SIZE]);		/* allow user to enter ships	 			*/
void initialize_board(char [B_SIZE][B_SIZE], int);	/* displays equation in fashionable manner	*/
void display_board(char [B_SIZE][B_SIZE]);			/* displays equation in fashionable manner	*/
int player_turn(char **, int *, int *); // gives player oprtunity to fight (shooting phase)	
int enemy_turn(char **, int *, int *, int);	// places a mark on players board based on enemy originating cords
int save_boards_to_file(char [B_SIZE][B_SIZE]);		/* allow user to save equation to file		*/
int read_boards_from_file(char [B_SIZE][B_SIZE]);	/* allow user to read equation from file	*/


